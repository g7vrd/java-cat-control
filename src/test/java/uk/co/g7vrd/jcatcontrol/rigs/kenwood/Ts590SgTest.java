package uk.co.g7vrd.jcatcontrol.rigs.kenwood;

import org.junit.Test;
import uk.co.g7vrd.jcatcontrol.DummySerialPort;
import uk.co.g7vrd.jcatcontrol.operations.BusyStatus;
import uk.co.g7vrd.jcatcontrol.operations.Frequency;
import uk.co.g7vrd.jcatcontrol.operations.Mode;

import static org.assertj.core.api.Assertions.assertThat;

public class Ts590SgTest {

  @Test
  public void TS590SG_ReadFrequency() {
    Ts590Sg ts590Sg = new Ts590Sg(new DummySerialPort("RI;", "RI;12345678901234;"));

    Frequency frequency = ts590Sg.readFrequency();

    assertThat(frequency.getHz()).isEqualTo(1234567890L);
  }

  @Test
  public void TS590SG_ReadMode_CW() {
    Ts590Sg ts590Sg = new Ts590Sg(new DummySerialPort("MD;", "MD3;"));

    Mode mode = ts590Sg.readMode();

    assertThat(mode).isEqualByComparingTo(Mode.CW);
  }

  @Test
  public void TS590SG_ReadMode_USB() {
    Ts590Sg ts590Sg = new Ts590Sg(new DummySerialPort("MD;", "MD2;"));

    Mode mode = ts590Sg.readMode();

    assertThat(mode).isEqualByComparingTo(Mode.USB);
  }

  @Test
  public void TS590SG_ReadBusyStatus_BUSY() {
    Ts590Sg ts590Sg = new Ts590Sg(new DummySerialPort("BY;", "BY10;"));

    BusyStatus busyStatus = ts590Sg.readBusyStatus();

    assertThat(busyStatus).isEqualByComparingTo(BusyStatus.BUSY);
  }
}
