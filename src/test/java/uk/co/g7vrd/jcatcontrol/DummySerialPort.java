package uk.co.g7vrd.jcatcontrol;

import uk.co.g7vrd.jcatcontrol.serial.SerialPort;

import static org.assertj.core.api.Assertions.assertThat;

public class DummySerialPort implements SerialPort {

  private final String expect;
  private final String response;

  public DummySerialPort(String expect, String response) {
    this.expect = expect;
    this.response = response;
  }

  @Override
  public int write(byte[] bytes) {
    assertThat(expect).isEqualTo(new String(bytes));
    return bytes.length;
  }

  @Override
  public int read(byte[] bytes, int numBytes) {
    System.arraycopy(response.getBytes(), 0, bytes, 0, numBytes);
    return bytes.length;
  }
}
