package uk.co.g7vrd.jcatcontrol.serial;

import uk.co.g7vrd.jcatcontrol.JCatControl;

import static com.fazecast.jSerialComm.SerialPort.NO_PARITY;
import static com.fazecast.jSerialComm.SerialPort.TIMEOUT_READ_BLOCKING;

/**
 * Implementation of {@link SerialPort} using com.fazecast.jSerialComm
 */
public class JSerialCommPort implements SerialPort {

  private final com.fazecast.jSerialComm.SerialPort commPort;

  public JSerialCommPort(String serialPort,
                         int baudRate,
                         int dataBits,
                         JCatControl.Parity parity,
                         int stopBits) {
    commPort = com.fazecast.jSerialComm.SerialPort.getCommPort(serialPort);
    commPort.setComPortParameters(baudRate, dataBits, stopBits, getParity(parity));
    commPort.setComPortTimeouts(TIMEOUT_READ_BLOCKING, 1, 2);
    commPort.openPort();
    if (!commPort.isOpen()) {
      System.err.println("Couldn't open " + serialPort);
      System.exit(1);
    }
    System.out.println("Comm port opened");
  }

  @Override
  public int write(byte[] bytes) {
    return commPort.writeBytes(bytes, bytes.length);
  }

  @Override
  public int read(byte[] bytes,
                  int numBytes) {
    if (bytes.length < numBytes) throw new RuntimeException("bytes is smaller than numBytes");
    return commPort.readBytes(bytes, numBytes);
  }

  private int getParity(JCatControl.Parity parity) {
    if (parity == JCatControl.Parity.NONE) return NO_PARITY;

    throw new RuntimeException("Don't understand " + parity);
  }

}
