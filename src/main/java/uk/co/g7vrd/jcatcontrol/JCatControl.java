package uk.co.g7vrd.jcatcontrol;

import uk.co.g7vrd.jcatcontrol.listeners.ChangeListener;
import uk.co.g7vrd.jcatcontrol.listeners.ChangeListeners;
import uk.co.g7vrd.jcatcontrol.listeners.OptionalChangeListener;
import uk.co.g7vrd.jcatcontrol.operations.*;
import uk.co.g7vrd.jcatcontrol.rigs.Rig;
import uk.co.g7vrd.jcatcontrol.rigs.kenwood.Ts590Sg;
import uk.co.g7vrd.jcatcontrol.serial.JSerialCommPort;
import uk.co.g7vrd.jcatcontrol.serial.SerialPort;

import java.time.Instant;
import java.util.Optional;

@SuppressWarnings("unused")
public class JCatControl extends Thread {

  private final int sleepMs;
  private volatile boolean running = true;
  private final Rig rig;

  // State
  // TODO: Make this better
  private Mode lastMode;
  private Frequency lastFrequency;
  private Optional<RxSignal> lastRxSignal = Optional.empty();
  private Optional<TxPower> lastTxPower = Optional.empty();
  private OutputPower lastOutputPower;
  private TxRxStatus lastTxRxStatus;
  private DataMode lastDataMode;
  private FilterWidth lastFilterWidth;
  private HighCut lastHighCut;
  private LowCut lastLowCut;
  private AlcMeter lastMeter;
  private InternalAntennaTunerStatus lastInternalAntennaTunerStatus;
  private SelectedAntenna lastSelectedAntenna;
  private Optional<SwrMeter> lastSwrMeter = Optional.empty();
  private Optional<CompressionMeter> lastCompressionMeter = Optional.empty();
  private Optional<AlcMeter> lastAlcMeter = Optional.empty();
  private BusyStatus lastBusyStatus;

  // Listeners
  private final ChangeListeners changeListeners = new ChangeListeners();

  @SuppressWarnings("WeakerAccess")
  public JCatControl(String rigId,
                     int sleepMs,
                     String serialPort,
                     int baudRate,
                     int dataBits,
                     Parity parity,
                     int stopBits) {
    this.sleepMs = sleepMs;
    System.out.printf("Initialising JHamLib: %s %d %s %d %d %s %d%n", rigId, sleepMs, serialPort, baudRate, dataBits, parity, stopBits);

    SerialPort serial = new JSerialCommPort(serialPort, baudRate, dataBits, parity, stopBits);

    // TODO: Find correct rig properly
    rig = new Ts590Sg(serial);
  }

  @Override
  public void run() {
    while (running) {
      if (rig instanceof ReadFrequency) {
        final Frequency current = ((ReadFrequency) rig).readFrequency();
        if (!current.equals(lastFrequency)) {
          changeListeners.getListeners(Frequency.class).forEach(listener -> listener.event(Instant.now(), lastFrequency, current));
          lastFrequency = current;
        }
      }

      if (rig instanceof ReadMode) {
        final Mode current = ((ReadMode) rig).readMode();
        if (!current.equals(lastMode)) {
          changeListeners.getListeners(Mode.class).forEach(listener -> listener.event(Instant.now(), lastMode, current));
          lastMode = current;
        }
      }

      if (rig instanceof ReadRxSignal) {
        final Optional<RxSignal> current = ((ReadRxSignal) rig).readRxSignal();
        if (!lastRxSignal.isPresent() && current.isPresent()) {
          changeListeners.getOptionalListeners(RxSignal.class).forEach(listener -> listener.event(Instant.now(), lastRxSignal, current));
          lastRxSignal = current;
        } else if (lastRxSignal.isPresent() && !current.isPresent()) {
          changeListeners.getOptionalListeners(RxSignal.class).forEach(listener -> listener.event(Instant.now(), lastRxSignal, current));
          lastRxSignal = current;
        } else if (lastRxSignal.isPresent() && current.isPresent()) {
          if (!current.get().equals(lastRxSignal.get())) {
            changeListeners.getOptionalListeners(RxSignal.class).forEach(listener -> listener.event(Instant.now(), lastRxSignal, current));
            lastRxSignal = current;
          }
        }
      }

      if (rig instanceof ReadTxPower) {
        final Optional<TxPower> current = ((ReadTxPower) rig).readTxPower();
        if (!lastTxPower.isPresent() && current.isPresent()) {
          changeListeners.getOptionalListeners(TxPower.class).forEach(listener -> listener.event(Instant.now(), lastTxPower, current));
          lastTxPower = current;
        } else if (lastTxPower.isPresent() && !current.isPresent()) {
          changeListeners.getOptionalListeners(TxPower.class).forEach(listener -> listener.event(Instant.now(), lastTxPower, current));
          lastTxPower = current;
        } else if (lastTxPower.isPresent() && current.isPresent()) {
          if (!current.get().equals(lastTxPower.get())) {
            changeListeners.getOptionalListeners(TxPower.class).forEach(listener -> listener.event(Instant.now(), lastTxPower, current));
            lastTxPower = current;
          }
        }
      }

      if (rig instanceof ReadOutputPower) {
        final OutputPower current = ((ReadOutputPower) rig).readOutputPower();
        if (!current.equals(lastOutputPower)) {
          changeListeners.getListeners(OutputPower.class).forEach(listener -> listener.event(Instant.now(), lastOutputPower, current));
          lastOutputPower = current;
        }
      }

      if (rig instanceof ReadTxRxStatus) {
        final TxRxStatus current = ((ReadTxRxStatus) rig).readTxRxStatus();
        if (!current.equals(lastTxRxStatus)) {
          changeListeners.getListeners(TxRxStatus.class).forEach(listener -> listener.event(Instant.now(), lastTxRxStatus, current));
          lastTxRxStatus = current;
        }
      }

      if (rig instanceof ReadDataMode) {
        final DataMode current = ((ReadDataMode) rig).readDataMode();
        if (!current.equals(lastDataMode)) {
          changeListeners.getListeners(DataMode.class).forEach(listener -> listener.event(Instant.now(), lastDataMode, current));
          lastDataMode = current;
        }
      }

      if (rig instanceof ReadFilterWidth) {
        final FilterWidth current = ((ReadFilterWidth) rig).readFilterWidth();
        if (!current.equals(lastFilterWidth)) {
          changeListeners.getListeners(FilterWidth.class).forEach(listener -> listener.event(Instant.now(), lastFilterWidth, current));
          lastFilterWidth = current;
        }
      }

      if (rig instanceof ReadHighCut) {
        final HighCut current = ((ReadHighCut) rig).readHighCut();
        if (!current.equals(lastHighCut)) {
          changeListeners.getListeners(HighCut.class).forEach(listener -> listener.event(Instant.now(), lastHighCut, current));
          lastHighCut = current;
        }
      }

      if (rig instanceof ReadLowCut) {
        final LowCut current = ((ReadLowCut) rig).readLowCut();
        if (!current.equals(lastLowCut)) {
          changeListeners.getListeners(LowCut.class).forEach(listener -> listener.event(Instant.now(), lastLowCut, current));
          lastLowCut = current;
        }
      }

      if (rig instanceof ReadSwrMeter) {
        final Optional<SwrMeter> current = ((ReadSwrMeter) rig).readSwrMeter();
        if (!lastSwrMeter.isPresent() && current.isPresent()) {
          changeListeners.getOptionalListeners(SwrMeter.class).forEach(listener -> listener.event(Instant.now(), lastSwrMeter, current));
          lastSwrMeter = current;
        } else if (lastSwrMeter.isPresent() && !current.isPresent()) {
          changeListeners.getOptionalListeners(SwrMeter.class).forEach(listener -> listener.event(Instant.now(), lastSwrMeter, current));
          lastSwrMeter = current;
        } else if (lastSwrMeter.isPresent() && current.isPresent()) {
          if (!current.get().equals(lastSwrMeter.get())) {
            changeListeners.getOptionalListeners(SwrMeter.class).forEach(listener -> listener.event(Instant.now(), lastSwrMeter, current));
            lastSwrMeter = current;
          }
        }
      }

      if (rig instanceof ReadCompressionMeter) {
        final Optional<CompressionMeter> current = ((ReadCompressionMeter) rig).readCompressionMeter();
        if (!lastCompressionMeter.isPresent() && current.isPresent()) {
          changeListeners.getOptionalListeners(CompressionMeter.class).forEach(listener -> listener.event(Instant.now(), lastCompressionMeter, current));
          lastCompressionMeter = current;
        } else if (lastCompressionMeter.isPresent() && !current.isPresent()) {
          changeListeners.getOptionalListeners(CompressionMeter.class).forEach(listener -> listener.event(Instant.now(), lastCompressionMeter, current));
          lastCompressionMeter = current;
        } else if (lastCompressionMeter.isPresent() && current.isPresent()) {
          if (!current.get().equals(lastCompressionMeter.get())) {
            changeListeners.getOptionalListeners(CompressionMeter.class).forEach(listener -> listener.event(Instant.now(), lastCompressionMeter, current));
            lastCompressionMeter = current;
          }
        }
      }

      if (rig instanceof ReadAlcMeter) {
        final Optional<AlcMeter> current = ((ReadAlcMeter) rig).readAlcMeter();
        if (!lastAlcMeter.isPresent() && current.isPresent()) {
          changeListeners.getOptionalListeners(AlcMeter.class).forEach(listener -> listener.event(Instant.now(), lastAlcMeter, current));
          lastAlcMeter = current;
        } else if (lastAlcMeter.isPresent() && !current.isPresent()) {
          changeListeners.getOptionalListeners(AlcMeter.class).forEach(listener -> listener.event(Instant.now(), lastAlcMeter, current));
          lastAlcMeter = current;
        } else if (lastAlcMeter.isPresent() && current.isPresent()) {
          if (!current.get().equals(lastAlcMeter.get())) {
            changeListeners.getOptionalListeners(AlcMeter.class).forEach(listener -> listener.event(Instant.now(), lastAlcMeter, current));
            lastAlcMeter = current;
          }
        }
      }

      if (rig instanceof ReadInternalAntennaTunerStatus) {
        final InternalAntennaTunerStatus current = ((ReadInternalAntennaTunerStatus) rig).readInternalAntennaTunerStatus();
        if (!current.equals(lastInternalAntennaTunerStatus)) {
          changeListeners.getListeners(InternalAntennaTunerStatus.class).forEach(listener -> listener.event(Instant.now(), lastInternalAntennaTunerStatus, current));
          lastInternalAntennaTunerStatus = current;
        }
      }

      if (rig instanceof ReadSelectedAntenna) {
        final SelectedAntenna current = ((ReadSelectedAntenna) rig).readSelectedAntenna();
        if (!current.equals(lastSelectedAntenna)) {
          changeListeners.getListeners(SelectedAntenna.class).forEach(listener -> listener.event(Instant.now(), lastSelectedAntenna, current));
          lastSelectedAntenna = current;
        }
      }

      if (rig instanceof ReadBusyStatus) {
        final BusyStatus current = ((ReadBusyStatus) rig).readBusyStatus();
        if (!current.equals(lastBusyStatus)) {
          changeListeners.getListeners(current.getClass()).forEach(listener ->
            listener.event(Instant.now(), lastBusyStatus, current));
          lastBusyStatus = current;
        }
      }

      zzz(sleepMs);
    }
  }

  public void startRig() {
    this.start();
  }

  public void stopRig() {
    running = false;
  }

  public void addChangeListener(Class<? extends Monitorable> monitorable, ChangeListener<? extends Monitorable> changeListener) {
    changeListeners.addChangeListener(monitorable, changeListener);
  }

  public void addOptionalChangeListener(Class<? extends Monitorable> monitorable, OptionalChangeListener<? extends Monitorable> optionalChangeListener) {
    changeListeners.addOptionalChangeListener(monitorable, optionalChangeListener);
  }


  private void zzz(@SuppressWarnings("SameParameterValue") int ms) {
    try {
      Thread.sleep(ms);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
  }

  public enum Parity {
    NONE,
    ODD,
    EVEN,
    MARK,
    SPACE
  }
}
