package uk.co.g7vrd.jcatcontrol.operations;

import java.util.Optional;

public interface ReadAlcMeter {

  Optional<AlcMeter> readAlcMeter();
}
