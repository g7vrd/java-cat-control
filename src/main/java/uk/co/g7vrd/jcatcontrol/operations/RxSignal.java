package uk.co.g7vrd.jcatcontrol.operations;

import java.util.Objects;

public class RxSignal implements Monitorable {

  private final int db;

  public RxSignal(int db) {
    this.db = db;
  }

  public int getDb() {
    return db;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    RxSignal signal = (RxSignal) o;
    return db == signal.db;
  }

  @Override
  public int hashCode() {
    return Objects.hash(db);
  }
}
