package uk.co.g7vrd.jcatcontrol.operations;

import java.util.Objects;

public class OutputPower implements Monitorable {

  private final int watts;

  public OutputPower(int watts) {
    this.watts = watts;
  }

  public int getWatts() {
    return watts;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    OutputPower that = (OutputPower) o;
    return watts == that.watts;
  }

  @Override
  public int hashCode() {
    return Objects.hash(watts);
  }
}
