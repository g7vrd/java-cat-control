package uk.co.g7vrd.jcatcontrol.operations;

public interface ReadBusyStatus {

  BusyStatus readBusyStatus();
}
