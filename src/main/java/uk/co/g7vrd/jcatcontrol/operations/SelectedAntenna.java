package uk.co.g7vrd.jcatcontrol.operations;

import java.util.Objects;

public class SelectedAntenna implements Monitorable {

  private final int antenna;

  public SelectedAntenna(int antenna) {
    this.antenna = antenna;
  }

  public int getAntenna() {
    return antenna;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    SelectedAntenna that = (SelectedAntenna) o;
    return antenna == that.antenna;
  }

  @Override
  public int hashCode() {
    return Objects.hash(antenna);
  }
}
