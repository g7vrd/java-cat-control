package uk.co.g7vrd.jcatcontrol.operations;

import java.util.Objects;

public class AlcMeter implements Monitorable {

  private final int alc;

  public AlcMeter(int alc) {
    this.alc = alc;
  }

  public int getAlc() {
    return alc;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AlcMeter alcMeter = (AlcMeter) o;
    return alc == alcMeter.alc;
  }

  @Override
  public int hashCode() {
    return Objects.hash(alc);
  }
}
