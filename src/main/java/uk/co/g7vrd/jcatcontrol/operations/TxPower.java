package uk.co.g7vrd.jcatcontrol.operations;

import java.util.Objects;

public class TxPower implements Monitorable {

  private final int watts;

  public TxPower(int watts) {
    this.watts = watts;
  }

  public int getWatts() {
    return watts;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    TxPower txPower = (TxPower) o;
    return watts == txPower.watts;
  }

  @Override
  public int hashCode() {
    return Objects.hash(watts);
  }
}
