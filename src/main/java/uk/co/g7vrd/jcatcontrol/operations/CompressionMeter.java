package uk.co.g7vrd.jcatcontrol.operations;

import java.util.Objects;

public class CompressionMeter implements Monitorable {

  private final int compression;

  public CompressionMeter(int compression) {
    this.compression = compression;
  }

  public int getCompression() {
    return compression;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CompressionMeter that = (CompressionMeter) o;
    return compression == that.compression;
  }

  @Override
  public int hashCode() {
    return Objects.hash(compression);
  }
}
