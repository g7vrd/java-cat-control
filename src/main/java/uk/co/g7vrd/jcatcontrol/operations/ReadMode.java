package uk.co.g7vrd.jcatcontrol.operations;

public interface ReadMode {

  Mode readMode();

}
