package uk.co.g7vrd.jcatcontrol.operations;

import java.util.Optional;

public interface ReadTxPower {

  /**
   * Returns the currently transmitted power in Watts
   * Can be Optional.empty() if not transmitting
   *
   * @see ReadOutputPower for the currently configured output power
   */
  Optional<TxPower> readTxPower();

}
