package uk.co.g7vrd.jcatcontrol.operations;

public enum TxRxStatus implements Monitorable {

  RECEIVING,
  TRANSMITTING
}
