package uk.co.g7vrd.jcatcontrol.operations;

public interface ReadDataMode {

  /**
   * Returns whether the Data mode is enabled or not
   */
  DataMode readDataMode();
}
