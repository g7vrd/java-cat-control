package uk.co.g7vrd.jcatcontrol.operations;

import java.util.Objects;

public class HighCut implements Monitorable {

  private final int hz;

  public HighCut(int hz) {
    this.hz = hz;
  }

  public int getHz() {
    return hz;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    HighCut highCut = (HighCut) o;
    return hz == highCut.hz;
  }

  @Override
  public int hashCode() {
    return Objects.hash(hz);
  }
}
