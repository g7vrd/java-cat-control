package uk.co.g7vrd.jcatcontrol.operations;

import java.util.Optional;

public interface ReadRxSignal {

  /**
   * Returns the currently received signal strength in dB
   * Can be Optional.empty() if not receiving
   */
  Optional<RxSignal> readRxSignal();

}
