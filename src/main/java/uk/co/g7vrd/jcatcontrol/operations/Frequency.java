package uk.co.g7vrd.jcatcontrol.operations;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class Frequency implements Monitorable {

  private final long hz;

  public Frequency(long hz) {
    this.hz = hz;
  }

  public long getHz() {
    return hz;
  }

  public BigDecimal getMhz() {
    return new BigDecimal(hz).divide(new BigDecimal(1_000_000), 6, RoundingMode.HALF_UP);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Frequency frequency = (Frequency) o;
    return hz == frequency.hz;
  }

  @Override
  public int hashCode() {
    return Objects.hash(hz);
  }
}
