package uk.co.g7vrd.jcatcontrol.operations;

public enum Mode implements Monitorable {

  CW,
  LSB,
  USB,
  FM,
  AM,
  FSK,
  CW_R,
  FSK_R
}
