package uk.co.g7vrd.jcatcontrol.operations;

public interface ReadFilterWidth {

  FilterWidth readFilterWidth();
}
