package uk.co.g7vrd.jcatcontrol.operations;

public enum BusyStatus implements Monitorable {

  NOT_BUSY,
  BUSY
}
