package uk.co.g7vrd.jcatcontrol.operations;

public interface ReadFrequency {

  /**
   * Returns the current frequency of the radio in Hz
   */
  Frequency readFrequency();
}
