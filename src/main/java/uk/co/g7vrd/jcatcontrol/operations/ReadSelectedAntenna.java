package uk.co.g7vrd.jcatcontrol.operations;

public interface ReadSelectedAntenna {

  SelectedAntenna readSelectedAntenna();
}
