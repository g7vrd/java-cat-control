package uk.co.g7vrd.jcatcontrol.operations;

public interface SetFrequency {

  void setFrequency(Frequency frequency);

}
