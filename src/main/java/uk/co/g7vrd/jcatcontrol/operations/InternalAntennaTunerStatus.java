package uk.co.g7vrd.jcatcontrol.operations;

import java.util.Objects;

public class InternalAntennaTunerStatus implements Monitorable {

  private boolean rx;
  private boolean tx;
  private boolean tuning;

  public void setRx(boolean rx) {
    this.rx = rx;
  }

  public boolean getRx() {
    return rx;
  }

  public void setTx(boolean tx) {
    this.tx = tx;
  }

  public boolean getTx() {
    return tx;
  }

  public void setTuning(boolean tuning) {
    this.tuning = tuning;
  }

  public boolean getTuning() {
    return tuning;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    InternalAntennaTunerStatus that = (InternalAntennaTunerStatus) o;
    return rx == that.rx &&
      tx == that.tx &&
      tuning == that.tuning;
  }

  @Override
  public int hashCode() {
    return Objects.hash(rx, tx, tuning);
  }
}
