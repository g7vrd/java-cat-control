package uk.co.g7vrd.jcatcontrol.operations;

public interface ReadInternalAntennaTunerStatus {

  /**
   * Returns the current status of the internal antenna tuner
   */
  InternalAntennaTunerStatus readInternalAntennaTunerStatus();
}
