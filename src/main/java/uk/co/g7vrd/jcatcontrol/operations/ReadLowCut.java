package uk.co.g7vrd.jcatcontrol.operations;

public interface ReadLowCut {

  LowCut readLowCut();
}
