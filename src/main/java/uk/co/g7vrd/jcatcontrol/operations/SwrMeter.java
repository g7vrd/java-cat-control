package uk.co.g7vrd.jcatcontrol.operations;

import java.util.Objects;

public class SwrMeter implements Monitorable {

  // TODO: Is float OK?
  private final float swr;

  public SwrMeter(float swr) {
    this.swr = swr;
  }

  public float getSwr() {
    return swr;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    SwrMeter swrMeter = (SwrMeter) o;
    return Float.compare(swrMeter.swr, swr) == 0;
  }

  @Override
  public int hashCode() {
    return Objects.hash(swr);
  }
}
