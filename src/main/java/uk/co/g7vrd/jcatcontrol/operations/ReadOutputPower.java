package uk.co.g7vrd.jcatcontrol.operations;

public interface ReadOutputPower {

  /**
   * Returns the currently configured transmitter power in Watts.
   * Note: When transmitting, the power may be less than this.
   *
   * @see ReadTxPower to see the current transmitting power
   */
  OutputPower readOutputPower();

}
