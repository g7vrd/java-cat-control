package uk.co.g7vrd.jcatcontrol.operations;

public enum DataMode implements Monitorable {

  OFF,
  ON
}
