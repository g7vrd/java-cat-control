package uk.co.g7vrd.jcatcontrol.operations;

public interface ReadHighCut {

  HighCut readHighCut();
}
