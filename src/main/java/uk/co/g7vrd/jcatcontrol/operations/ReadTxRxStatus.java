package uk.co.g7vrd.jcatcontrol.operations;

public interface ReadTxRxStatus {

  TxRxStatus readTxRxStatus();

}
