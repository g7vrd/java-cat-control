package uk.co.g7vrd.jcatcontrol;

public class JCatControlBuilder {

  private String rigId;
  private String serialPort = "ttyUSB0";
  private int baudRate = 115200;
  private int dataBits = 8;
  private JCatControl.Parity parity = JCatControl.Parity.NONE;
  private int stopBits = 1;
  private int sleepMs = 100;

  public JCatControlBuilder rigId(String rigId) {
    this.rigId = rigId;
    return this;
  }

  public JCatControlBuilder serialPort(String serialPort) {
    this.serialPort = serialPort;
    return this;
  }

  public JCatControlBuilder baudRate(int baudRate) {
    this.baudRate = baudRate;
    return this;
  }

  public JCatControlBuilder dataBits(int dataBits) {
    this.dataBits = dataBits;
    return this;
  }

  public JCatControlBuilder parity(JCatControl.Parity parity) {
    this.parity = parity;
    return this;
  }

  public JCatControlBuilder stopBits(int stopBits) {
    this.stopBits = stopBits;
    return this;
  }

  public JCatControlBuilder sleepMs(int sleepMs) {
    this.sleepMs = sleepMs;
    return this;
  }

  public JCatControl build() {
    return new JCatControl(rigId, sleepMs, serialPort, baudRate, dataBits, parity, stopBits);
  }
}
