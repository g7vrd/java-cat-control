package uk.co.g7vrd.jcatcontrol.listeners;

import uk.co.g7vrd.jcatcontrol.operations.Monitorable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ChangeListeners {

  private final Map<Class<? extends Monitorable>, Collection<ChangeListener<? extends Monitorable>>> listeners = new ConcurrentHashMap<>();
  private final Map<Class<? extends Monitorable>, Collection<OptionalChangeListener<? extends Monitorable>>> optionalListeners = new ConcurrentHashMap<>();

  public Collection<ChangeListener<? extends Monitorable>> getListeners(Class<? extends Monitorable> monitorable) {
    return listeners.getOrDefault(monitorable, new ArrayList<>());
  }

  public Collection<OptionalChangeListener<? extends Monitorable>> getOptionalListeners(Class<? extends Monitorable> monitorable) {
    return optionalListeners.getOrDefault(monitorable, new ArrayList<>());
  }

  public void addChangeListener(Class<? extends Monitorable> monitorable, ChangeListener<? extends Monitorable> changeListener) {
    Collection<ChangeListener<? extends Monitorable>> listeners = getListeners(monitorable);
    listeners.add(changeListener);
    this.listeners.put(monitorable, listeners);
  }

  public void addOptionalChangeListener(Class<? extends Monitorable> monitorable, OptionalChangeListener<? extends Monitorable> changeListener) {
    Collection<OptionalChangeListener<? extends Monitorable>> optionalListeners = getOptionalListeners(monitorable);
    optionalListeners.add(changeListener);
    this.optionalListeners.put(monitorable, optionalListeners);
  }
}
