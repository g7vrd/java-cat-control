package uk.co.g7vrd.jcatcontrol.listeners;

import uk.co.g7vrd.jcatcontrol.operations.Monitorable;

import java.time.Instant;

public interface ChangeListener<T extends Monitorable> {

  /**
   * Called when a change for {@link Monitorable} has occurred
   */
  void event(Instant now, Monitorable last, Monitorable current);
}
