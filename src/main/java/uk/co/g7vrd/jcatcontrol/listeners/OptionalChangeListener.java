package uk.co.g7vrd.jcatcontrol.listeners;

import uk.co.g7vrd.jcatcontrol.operations.Monitorable;

import java.time.Instant;
import java.util.Optional;

public interface OptionalChangeListener<T extends Monitorable> {

  /**
   * Called when a change for {@link Monitorable} has occurred
   */
  @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
  void event(Instant now, Optional<? extends Monitorable> last, Optional<? extends Monitorable> current);
}
