package uk.co.g7vrd.jcatcontrol.rigs.kenwood.ts590sg;

import uk.co.g7vrd.jcatcontrol.operations.RxSignal;

import java.util.Optional;

public class DotsToRxSignal {

  public static Optional<RxSignal> toRxSignal(int dots) {
    // TODO: Find out the correct mappings
    if (dots == 0) return Optional.of(new RxSignal(0));
    if (dots >= 1 && dots <= 3) return Optional.of(new RxSignal((1)));
    if (dots >= 4 && dots <= 5) return Optional.of(new RxSignal((2)));
    if (dots == 6) return Optional.of(new RxSignal((3)));
    if (dots >= 7 && dots <= 8) return Optional.of(new RxSignal((4)));
    if (dots == 9) return Optional.of(new RxSignal((5)));
    if (dots >= 10 && dots <= 11) return Optional.of(new RxSignal((6)));
    if (dots == 12) return Optional.of(new RxSignal((7)));
    if (dots >= 13 && dots <= 14) return Optional.of(new RxSignal((8)));
    if (dots == 15) return Optional.of(new RxSignal((9)));
    if (dots >= 16 && dots <= 19) return Optional.of(new RxSignal((10)));
    if (dots == 20) return Optional.of(new RxSignal((20)));
    if (dots >= 21 && dots <= 24) return Optional.of(new RxSignal((30)));
    if (dots == 25) return Optional.of(new RxSignal((40)));
    if (dots >= 26 && dots <= 29) return Optional.of(new RxSignal((50)));
    if (dots == 30) return Optional.of(new RxSignal((60)));

    throw new RuntimeException("No RxSignal for " + dots);
  }
}
