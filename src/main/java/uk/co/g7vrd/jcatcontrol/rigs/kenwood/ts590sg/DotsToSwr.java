package uk.co.g7vrd.jcatcontrol.rigs.kenwood.ts590sg;

import uk.co.g7vrd.jcatcontrol.operations.SwrMeter;

public class DotsToSwr {

  public static SwrMeter toSwr(int dots) {
    // TODO: Find out the correct mappings
    switch (dots) {
      case 0 : return new SwrMeter(1F);
      case 1 : return new SwrMeter(1F);
      case 2 : return new SwrMeter(1.1F);
      case 3 : return new SwrMeter(1.2F);
      case 4 : return new SwrMeter(1.3F);
      case 5 : return new SwrMeter(1.4F);
      case 6 : return new SwrMeter(1.5F);
      case 7 : return new SwrMeter(1.6F);
      case 8 : return new SwrMeter(1.7F);
      case 9 : return new SwrMeter(1.8F);
      case 10 : return new SwrMeter(1.9F);
      case 11 : return new SwrMeter(2F);
      case 12 : return new SwrMeter(2.2F);
      case 13 : return new SwrMeter(2.4F);
      case 14 : return new SwrMeter(2.7F);
      case 15 : return new SwrMeter(3F);
      case 16 : return new SwrMeter(3.5F);
      case 17 : return new SwrMeter(4F);
      case 18 : return new SwrMeter(5F);
      case 19 : return new SwrMeter(10F);
      case 20 : return new SwrMeter(15F);
      case 21 : return new SwrMeter(20F);
      case 22 : return new SwrMeter(25F);
      case 23 : return new SwrMeter(50F);
      case 24 : return new SwrMeter(75F);
      case 25 : return new SwrMeter(100F);
      case 26 : return new SwrMeter(150F);
      case 27 : return new SwrMeter(200F);
      case 28 : return new SwrMeter(250F);
      case 29 : return new SwrMeter(500F);
      case 30 : return new SwrMeter(1000F);
    }

    throw new RuntimeException("No SWR for " + dots);
  }
}
