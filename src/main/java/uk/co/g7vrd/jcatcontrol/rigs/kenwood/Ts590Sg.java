package uk.co.g7vrd.jcatcontrol.rigs.kenwood;

import uk.co.g7vrd.jcatcontrol.operations.*;
import uk.co.g7vrd.jcatcontrol.rigs.Rig;
import uk.co.g7vrd.jcatcontrol.rigs.kenwood.ts590sg.DotsToRxSignal;
import uk.co.g7vrd.jcatcontrol.rigs.kenwood.ts590sg.DotsToSwr;
import uk.co.g7vrd.jcatcontrol.rigs.kenwood.ts590sg.DotsToTxPower;
import uk.co.g7vrd.jcatcontrol.serial.SerialPort;

import java.util.Optional;
import java.util.function.Consumer;

import static uk.co.g7vrd.jcatcontrol.operations.DataMode.OFF;
import static uk.co.g7vrd.jcatcontrol.operations.DataMode.ON;

public class Ts590Sg extends Rig implements KenwoodRig,
  ReadFrequency,
  ReadMode,
  ReadRxSignal,
  ReadOutputPower,
  ReadTxRxStatus,
  ReadDataMode,
  ReadAlcMeter,
  ReadSwrMeter,
  ReadCompressionMeter,
  ReadInternalAntennaTunerStatus,
  ReadSelectedAntenna,
  ReadTxPower,
  ReadBusyStatus {

  // https://www.kenwood.com/i/products/info/amateur/pdf/ts_590_g_pc_command_e.pdf

  private static final byte[] READ_STATUS = "IF;".getBytes();
  private static final byte[] READ_FREQUENCY = "RI;".getBytes();
  private static final byte[] READ_MODE = "MD;".getBytes();
  private static final byte[] READ_SIGNAL = "SM0;".getBytes();
  private static final byte[] READ_OUTPUT_POWER = "PC;".getBytes();
  private static final byte[] READ_BUSY = "BY;".getBytes();
  private static final byte[] READ_FILTER_WIDTH = "FW;".getBytes();
  private static final byte[] READ_FILTER_SHIFT = "IS;".getBytes();
  private static final byte[] READ_FILTER_CUTOFF_LOW = "SL;".getBytes();
  private static final byte[] READ_FILTER_CUTOFF_HIGH = "SH;".getBytes();
  private static final byte[] READ_DATA = "DA;".getBytes();
  private static final byte[] READ_METER = "RM;".getBytes();
  private static final byte[] READ_INTERNAL_ANTENNA_TUNER_STATUS = "AC;".getBytes();
  private static final byte[] READ_SELECTED_ANTENNA = "AN;".getBytes();


  private final Consumer<byte[]> readCheck = (bytes) -> {
    if (bytes[bytes.length - 1] != (byte) ';') {
      throw new RuntimeException("Bytes (" + new String(bytes) + ") read didn't end with ;");
    }
  };

  private TxRxStatus txRxStatus = TxRxStatus.RECEIVING;


  public Ts590Sg(SerialPort serial) {
    super(serial);
  }

  @Override
  public String getRigId() {
    return "TS590SG";
  }

  @Override
  public Frequency readFrequency() {
    write(READ_FREQUENCY);

    final String answer = new String(read(18, readCheck));

    return new Frequency(Long.parseLong(answer.substring(3, 13)));
  }

  @Override
  public Mode readMode() {
    write(READ_MODE);

    final String answer = new String(read(4, readCheck));
    final char modeChar = answer.charAt(2);
    if (modeChar == '1') return Mode.LSB;
    if (modeChar == '2') return Mode.USB;
    if (modeChar == '3') return Mode.CW;
    if (modeChar == '4') return Mode.FM;
    if (modeChar == '5') return Mode.AM;
    if (modeChar == '6') return Mode.FSK;
    if (modeChar == '7') return Mode.CW_R;
    if (modeChar == '9') return Mode.FSK_R;

    throw new RuntimeException("No Mode for " + modeChar);
  }

  @Override
  public Optional<RxSignal> readRxSignal() {
    if (txRxStatus == TxRxStatus.TRANSMITTING) {
      return Optional.empty();
    }
    write(READ_SIGNAL);

    final String answer = new String(read(8, readCheck));
    int dots = Integer.parseInt(answer.substring(4, 7));

    return DotsToRxSignal.toRxSignal(dots);
  }

  @Override
  public Optional<TxPower> readTxPower() {
    if (txRxStatus == TxRxStatus.RECEIVING) {
      return Optional.empty();
    }
    write(READ_SIGNAL);

    final String answer = new String(read(8, readCheck));
    int dots = Integer.parseInt(answer.substring(4, 7));

    return DotsToTxPower.toTxPower(dots);
  }


  @Override
  public OutputPower readOutputPower() {
    write(READ_OUTPUT_POWER);

    final String answer = new String(read(6, readCheck));

    int watts = Integer.parseInt(answer.substring(3, 5));
    if (watts == 0) watts = 100;
    return new OutputPower(watts);
  }

  @Override
  public TxRxStatus readTxRxStatus() {
    write(READ_STATUS);

    final String answer = new String(read(38, readCheck));

    if (answer.charAt(28) == '0') {
      txRxStatus = TxRxStatus.RECEIVING;
      return txRxStatus;
    }
    if (answer.charAt(28) == '1') {
      txRxStatus = TxRxStatus.TRANSMITTING;
      return txRxStatus;
    }

    throw new RuntimeException("No TxRxStatus for " + answer.charAt(28));
  }

  @Override
  public DataMode readDataMode() {
    write(READ_DATA);

    final String answer = new String(read(4, readCheck));

    if (answer.charAt(2) == '0') return OFF;
    if (answer.charAt(2) == '1') return ON;

    throw new RuntimeException("No DataMode for " + answer.charAt(2));
  }

  @Override
  public InternalAntennaTunerStatus readInternalAntennaTunerStatus() {
    write(READ_INTERNAL_ANTENNA_TUNER_STATUS);

    InternalAntennaTunerStatus internalAntennaTunerStatus = new InternalAntennaTunerStatus();

    final String answer = new String(read(6, readCheck));

    if (answer.charAt(2) == '0') internalAntennaTunerStatus.setRx(false);
    if (answer.charAt(2) == '1') internalAntennaTunerStatus.setRx(true);

    if (answer.charAt(3) == '0') internalAntennaTunerStatus.setTx(false);
    if (answer.charAt(3) == '1') internalAntennaTunerStatus.setTx(true);

    if (answer.charAt(4) == '0') internalAntennaTunerStatus.setTuning(false);
    if (answer.charAt(4) == '1') internalAntennaTunerStatus.setTuning(true);

    return internalAntennaTunerStatus;
  }

  @Override
  public SelectedAntenna readSelectedAntenna() {
    write(READ_SELECTED_ANTENNA);

    final String answer = new String(read(6, readCheck));

    if (answer.charAt(2) == '1') return new SelectedAntenna(1);
    if (answer.charAt(2) == '2') return new SelectedAntenna(2);

    throw new RuntimeException("No SelectedAntenna for " + answer.charAt(2));
  }

  @Override
  public Optional<AlcMeter> readAlcMeter() {
    if (txRxStatus == TxRxStatus.RECEIVING) {
      return Optional.empty();
    }
    write(READ_METER);

    read(8, readCheck);
    read(8, readCheck);
    final String alc = new String(read(8, readCheck));

    return Optional.of(new AlcMeter(Integer.parseInt(alc.substring(4, 7))));
  }

  @Override
  public Optional<CompressionMeter> readCompressionMeter() {
    if (txRxStatus == TxRxStatus.RECEIVING) {
      return Optional.empty();
    }
    write(READ_METER);

    read(8, readCheck);
    final String comp = new String(read(8, readCheck));
    read(8, readCheck);

    return Optional.of(new CompressionMeter(Integer.parseInt(comp.substring(4, 7))));
  }

  @Override
  public Optional<SwrMeter> readSwrMeter() {
    if (txRxStatus == TxRxStatus.RECEIVING) {
      return Optional.empty();
    }
    write(READ_METER);

    final String swr = new String(read(8, readCheck));
    read(8, readCheck);
    read(8, readCheck);

    return Optional.of(DotsToSwr.toSwr(Integer.parseInt(swr.substring(4, 7))));
  }

  @Override
  public BusyStatus readBusyStatus() {
    write(READ_BUSY);

    String answer = new String(read(5, readCheck));

    if (answer.charAt(2) == '0') return BusyStatus.NOT_BUSY;
    if (answer.charAt(2) == '1') return BusyStatus.BUSY;

    throw new RuntimeException("No BusyStatus for " + answer.charAt(2));
  }
}
