package uk.co.g7vrd.jcatcontrol.rigs.kenwood.ts590sg;

import uk.co.g7vrd.jcatcontrol.operations.TxPower;

import java.util.Optional;

public class DotsToTxPower {

  public static Optional<TxPower> toTxPower(int dots) {
    // TODO: Find out the correct mappings
    switch (dots) {
      case 0 : return Optional.of(new TxPower(0));
      case 1 : return Optional.of(new TxPower(2));
      case 2 : return Optional.of(new TxPower(4));
      case 3 : return Optional.of(new TxPower(6));
      case 4 : return Optional.of(new TxPower(7));
      case 5 : return Optional.of(new TxPower(8));
      case 6 : return Optional.of(new TxPower(10));
      case 7 : return Optional.of(new TxPower(13));
      case 8 : return Optional.of(new TxPower(16));
      case 9 : return Optional.of(new TxPower(19));
      case 10 : return Optional.of(new TxPower(21));
      case 11 : return Optional.of(new TxPower(23));
      case 12 : return Optional.of(new TxPower(25));
      case 13 : return Optional.of(new TxPower(29));
      case 14 : return Optional.of(new TxPower(33));
      case 15 : return Optional.of(new TxPower(37));
      case 16 : return Optional.of(new TxPower(40));
      case 17 : return Optional.of(new TxPower(45));
      case 18 : return Optional.of(new TxPower(50));
      case 19 : return Optional.of(new TxPower(54));
      case 20 : return Optional.of(new TxPower(58));
      case 21 : return Optional.of(new TxPower(62));
      case 22 : return Optional.of(new TxPower(66));
      case 23 : return Optional.of(new TxPower(70));
      case 24 : return Optional.of(new TxPower(74));
      case 25 : return Optional.of(new TxPower(78));
      case 26 : return Optional.of(new TxPower(82));
      case 27 : return Optional.of(new TxPower(86));
      case 28 : return Optional.of(new TxPower(90));
      case 29 : return Optional.of(new TxPower(95));
      case 30 : return Optional.of(new TxPower(100));
    }
    throw new RuntimeException("No TxPower for " + dots);
  }
}
