package uk.co.g7vrd.jcatcontrol.rigs.yaesu;

import uk.co.g7vrd.jcatcontrol.rigs.Rig;
import uk.co.g7vrd.jcatcontrol.serial.SerialPort;

public class Ft857D extends Rig implements YaesuRig {

  public Ft857D(SerialPort serialPort) {
    super(serialPort);
  }

  @Override
  public String getRigId() {
    return "FT857D";
  }
}
