package uk.co.g7vrd.jcatcontrol.rigs;


import uk.co.g7vrd.jcatcontrol.serial.SerialPort;

import java.util.function.Consumer;

public abstract class Rig {

  private final SerialPort serialPort;

  public Rig(SerialPort serialPort) {
    this.serialPort = serialPort;
  }

  public abstract String getRigId();

  public void write(byte[] bytes) {
    final int bytesWritten = serialPort.write(bytes);
    if (bytesWritten != bytes.length) throw new RuntimeException("Write error: " + bytesWritten);
  }

  public byte[] read(int length,
                     Consumer<byte[]> readCheck) {
    byte[] readBytes = new byte[length];
    final int bytesRead = serialPort.read(readBytes, readBytes.length);
    if (bytesRead != readBytes.length)
      throw new RuntimeException("Read error. Expected to read " + length + " but only read " + bytesRead +
        " (" + new String(readBytes) + ")");
    readCheck.accept(readBytes);

    return readBytes;
  }

}
