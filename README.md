This is a pure Java library to provide CAT control for transceivers.
It aims to be easy to add support for new radios.

It currently supports:
 * TS590SG
 
Latest version:

    <dependency>
        <groupId>uk.co.g7vrd</groupId>
        <artifactId>java-cat-control</artifactId>
        <version>0.0.5</version>
    </dependency>
